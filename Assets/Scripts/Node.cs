﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    public int id;
    public Node[] neighbours;
    public bool food;
    public bool home;
    public int foodAmount = 0;
}
