﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Ecosystem : MonoBehaviour {
    public static int id;
    public static int gen;

    public static int GENOME_SIZE = 26;
    public static List<GameObject> allOrganisms;
    public static List<Node> nodes;
    public static Node home;
    public static Node food;

    public Node initHome;
    public Node initFood;

    public List<Node> initNodes;

    public Material lineMaterial;
    public float maxX;
    public float maxY;
    public GameObject organism;
    private readonly int POP = 240;

    public Text generationNew;
    public Text generation;
    public Text generationOld;
    public Text fitness;
    public Text fitnessOld;



    void Awake() {
        nodes = initNodes;
        home = initHome;
        food = initFood;
    }

    // Start is called before the first frame update
    void Start() {
        id = 0;
        InitializeGraph();
        InitializeWorld();
        InvokeRepeating("Select", 5f, 1.5f);
    }

    void InitializeWorld() {
        allOrganisms = new List<GameObject>();
        for (int i = 0; i < POP; i++) {
            Vector2 randPos = home.transform.position + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 2);
            GameObject temp = Instantiate(organism, randPos, new Quaternion(90, 90, 90, 90));
            temp.GetComponent<CircleCollider2D>().enabled = true;
            temp.name = "Organism" + id++;
            temp.GetComponent<Movement>().genome = GenerateGenome();

            allOrganisms.Add(temp);
        }
    }

    void InitializeGraph() {
        foreach (Node n in nodes) {
            foreach (Node nn in n.neighbours) {
                DrawLine(n.transform.position, nn.transform.position);
            }
        }
    }


    void DrawLine(Vector3 start, Vector3 end) {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = lineMaterial;
        lr.startColor = Color.white;
        lr.startWidth = 5f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        myLine.name = "Tunnel";
    }

    bool AllStopped() {
        foreach (GameObject org in allOrganisms) {
            if (org.GetComponent<Movement>().running) {
                return false;
            }
        }
        return true;
    }

    void Select() {
        if (AllStopped()) {
            gen++;
            float avgFitness = 0;
            foreach (GameObject org in allOrganisms) {
                avgFitness += org.GetComponent<Movement>().fitness;
            }
            avgFitness /= allOrganisms.Count + 1;
            fitnessOld.text = fitness.text;
            fitness.text = ((int)avgFitness).ToString();
            List<GameObject> survivors = new List<GameObject>();
            List<GameObject> toClear = new List<GameObject>();

            allOrganisms.Sort((o1, o2) => o2.GetComponent<Movement>().fitness.CompareTo(o1.GetComponent<Movement>().fitness));
            survivors.AddRange(allOrganisms.GetRange(0, allOrganisms.Count / 6));
            toClear.AddRange(allOrganisms.GetRange(allOrganisms.Count / 6, allOrganisms.Count - allOrganisms.Count / 6));

            allOrganisms.Clear();
            allOrganisms.AddRange(survivors);
            foreach (GameObject org in toClear) {
                Destroy(org);
            }

            foreach (GameObject org in survivors) {
                allOrganisms.Add(org.GetComponent<Movement>().Multiply());
                allOrganisms.Add(org.GetComponent<Movement>().Multiply());
                allOrganisms.Add(org.GetComponent<Movement>().Multiply());
                allOrganisms.Add(org.GetComponent<Movement>().Multiply());
                allOrganisms.Add(org.GetComponent<Movement>().Multiply());
            }

            survivors.Clear();
            toClear.Clear();
            Restart();
        }
    }

    void Restart() {
        foreach (GameObject org in allOrganisms) {
            Vector2 randPos = home.transform.position + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 2);
            org.transform.position = randPos;
            org.GetComponent<Movement>().fitness = 0;
            org.GetComponent<Movement>().carryFood = false;
            org.GetComponent<Renderer>().material.color = Color.red;
            org.GetComponent<Movement>().curNode = home;
            org.GetComponent<Movement>().dstNode = null;
            org.GetComponent<Movement>().running = true;
            org.GetComponent<Movement>().stopInvoke = false;
            org.GetComponent<Movement>().foodOffset = 0;
        }
        generationOld.text = generation.text;
        generation.text = generationNew.text;
        generationNew.text = gen.ToString();
    }

    int[] GenerateGenome() {
        int[] genome = new int[GENOME_SIZE];
        int offset = 0;
        for (int i = 0; i < GENOME_SIZE; i++) {
            if (i == GENOME_SIZE / 2) {
                offset = GENOME_SIZE / 2;
            }
            genome[i] = Random.Range(0, nodes[i - offset].neighbours.Length);
        }

        return genome;
    }
}



