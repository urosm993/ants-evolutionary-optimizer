﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    public GameObject myPrefab;
    public int[] genome;
    public int fitness;
    [HideInInspector]
    public bool carryFood = false;
    [HideInInspector]
    public bool running = true;
    public Node curNode;
    public Node dstNode;
    [HideInInspector]
    public bool stopInvoke = false;


    private Renderer rend;
    private Vector3 dstPostion;
    readonly private int mutationFactor = 5;
    private int speed;
    public int foodOffset = 0;

    public int lookInto;


    void Start() {
        rend = GetComponent<Renderer>();
        curNode = Ecosystem.home;
        speed = Random.Range(80, 85);
        fitness = 0;
        dstNode = null;
    }

    // Update is called once per frame
    void Update() {
        if (stopInvoke == false) {
            Invoke("Stop", 10);
            stopInvoke = true;
        }
        if (running) {
            if (curNode.food && !carryFood) {
                fitness += (int)(Ecosystem.home.transform.position - Ecosystem.food.transform.position).magnitude;
                carryFood = true;
                foodOffset = Ecosystem.GENOME_SIZE / 2;
                rend.material.color = Color.yellow;
            }
            if (curNode.home && carryFood) {
                fitness += (int)(Ecosystem.home.transform.position - Ecosystem.food.transform.position).magnitude;
                carryFood = false;
                foodOffset = 0;
                rend.material.color = Color.blue;
                curNode.foodAmount++;
            }
            Move();
        }
    }

    void Move() {
        if (dstNode == null) {
            dstNode = curNode.neighbours[genome[curNode.id + foodOffset]];
            lookInto = curNode.id + foodOffset;
        } else {
            dstPostion = dstNode.transform.position + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 2);
            Vector3 directionToTarget = dstPostion - transform.position;
            float distanceToTarget = directionToTarget.magnitude;
            if (distanceToTarget < 3f) {
                curNode = dstNode;
                dstNode = null;
            } else {
                transform.position = Vector2.MoveTowards(transform.position, dstPostion, speed * Time.deltaTime);
            }
        }
    }

    void Stop() {
        Evaluate();
        running = false;
    }

    void Evaluate() {
        float dstToFood = (Ecosystem.food.transform.position - transform.position).magnitude;
        float dstToHome = (Ecosystem.home.transform.position - transform.position).magnitude;
        float startDistance = (Ecosystem.home.transform.position - Ecosystem.food.transform.position).magnitude;

        if (!carryFood) {
            fitness += ((int)startDistance - (int)dstToFood);
        } else {
            fitness += ((int)startDistance - (int)dstToHome);
        }
    }

    public GameObject Multiply() {
        GameObject offspring = Instantiate(myPrefab, transform.position + new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 2), transform.rotation) as GameObject;
        offspring.GetComponent<Movement>().enabled = true;
        offspring.name = Ecosystem.gen + " Organism " + Ecosystem.id++;
        offspring.GetComponent<Movement>().genome = (int[])genome.Clone();

        int offset = 0;
        for (int i = 0; i < Ecosystem.GENOME_SIZE; i++) {
            if (Random.Range(0, 100) > mutationFactor)
                continue;
            if (i >= Ecosystem.GENOME_SIZE / 2) {
                offset = Ecosystem.GENOME_SIZE / 2;
            }
            offspring.GetComponent<Movement>().genome[i] = Random.Range(0, Ecosystem.nodes[i - offset].neighbours.Length);
        }
        return offspring;
    }
}
